﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Vector3 StartPos;
    public Vector3 EndPos;
    public float TravelTime = 5;
    public float Delay = 0;

    private Rigidbody rigid;

    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
    }

    private IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForSeconds(Delay);
            yield return StartCoroutine(Forward());
            yield return new WaitForSeconds(Delay);
            yield return StartCoroutine(Back());
        }
    }

    private IEnumerator Forward()
    {
        float t = 0;
        while (t<TravelTime)
        {
            rigid.MovePosition(Vector3.Lerp(StartPos, EndPos, t/TravelTime));
            t += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        rigid.MovePosition(EndPos);
    }

    private IEnumerator Back()
    {
        float t = 0;
        while (t < TravelTime)
        {
            rigid.MovePosition(Vector3.Lerp(EndPos, StartPos, t / TravelTime));
            t += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        rigid.MovePosition(StartPos);
    }
}
