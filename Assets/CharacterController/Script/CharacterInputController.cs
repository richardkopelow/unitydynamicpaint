﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-1)]
public class CharacterInputController : MonoBehaviour
{
    public CharacterActions Actions;

    private void Awake()
    {
        if (Actions == null)
        {
            Actions = new CharacterActions();
        }
    }

    private void OnEnable()
    {
        Actions.Map.Enable();
    }

    private void OnDisable()
    {
        Actions.Map.Disable();
    }
}
