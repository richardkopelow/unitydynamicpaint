﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using static CharacterActions;
using static Unity.Mathematics.math;
using float3 = Unity.Mathematics.float3;
using quaternion = Unity.Mathematics.quaternion;

[RequireComponent(typeof(Rigidbody))]
public class CharacterController : MonoBehaviour
{
    public enum eAimType
    {
        FirstPerson = 1 << 0,
        ThirdPerson = 1 << 1,
        Hybrid = FirstPerson | ThirdPerson
    }
    public enum eMovementMode : int
    {
        Normal = 0,
        Sprinting = 1,
        Crouched = 2,
        Sliding = 3
    }
    public class AnimatorConstants
    {
        public static AnimatorID NormalizedVelocityX = "NormalizedVelocityX";
        public static AnimatorID NormalizedVelocityY = "NormalizedVelocityY";
        public static AnimatorID NormalizedVelocityZ = "NormalizedVelocityZ";
        public static AnimatorID Jump = "Jump";
        public static AnimatorID Grounded = "Grounded";
        public static AnimatorID MovementMode = "MovementMode";
        public static AnimatorID AimingMode = "AimingMode";
        public static AnimatorID Aiming = "Aiming";
        public static AnimatorID RightSholderCam = "RightCam";
    }
    public struct GroundData
    {
        public event System.Action<GameObject> OnGroundChanged;
        private GameObject _ground;
        public GameObject Ground
        {
            get => _ground;
            private set
            {
                _ground = value;
                Rigidbody = _ground?.GetComponent<Rigidbody>();
                OnGroundChanged?.Invoke(_ground);
            }
        }
        public Rigidbody Rigidbody { get; private set; }
        public PhysicMaterial PhysicMaterial { get; private set; }
        public float3 NormalForce { get; private set; }
        public float3 NormalVector { get; private set; }

        public void CollideWithGround(Collision collision, PhysicMaterial defaultPhysicMaterial)
        {
            Ground = collision.collider.gameObject;
            PhysicMaterial = collision.collider.sharedMaterial ?? defaultPhysicMaterial;
            NormalForce = collision.impulse / Time.fixedDeltaTime;
            NormalVector = collision.contacts[0].normal;
        }

        public void Clear()
        {
            Ground = null;
            NormalVector = Vector3.up;
            NormalForce = Vector3.zero;
        }
    }

    [System.Serializable]
    public class MovementSettings
    {
        public float MaxSpeed = 5;
        public float MaxAcceleration = 10;
        public float SlopeGroundingTolerance = 20;
        public float StairHeightTolerance = 0.1f;
        public float StairForceMultiplier = 1;
        public float JumpSpeed = 5;
        public float Height = 1.8f;
    }

    public PhysicMaterial DefaultPhysicMaterial;
    public Transform MovementReference;
    public Animator CamAnimator;
    [SerializeField]
    private eAimType _aimMode;
    public eAimType AimMode
    {
        get { return _aimMode; }
        set
        {
            _aimMode = value;
            anim?.SetInteger(AnimatorConstants.AimingMode, (int)_aimMode);
            CamAnimator?.SetInteger(AnimatorConstants.AimingMode, (int)_aimMode);
        }
    }

    private eMovementMode _currentMovementMode;
    public eMovementMode CurrentMovementMode
    {
        get { return _currentMovementMode; }
        set
        {
            _currentMovementMode = value;
            anim?.SetInteger(AnimatorConstants.MovementMode, (int)_currentMovementMode);
            CamAnimator?.SetInteger(AnimatorConstants.MovementMode, (int)_currentMovementMode);
            float height = CurrentMovementSettings.Height;
            collider.height = height;
            collider.center = new Vector3(0, height / 2, 0);
        }
    }

    public MovementSettings CurrentMovementSettings
    {
        get
        {
            switch (CurrentMovementMode)
            {
                case eMovementMode.Sprinting:
                    return SprintingSettings;
                case eMovementMode.Crouched:
                    return CrouchedSettings;
                case eMovementMode.Sliding:
                    return CrouchedSettings;
                default:
                    return NormalMovement;
            }
        }
    }
    public MovementSettings NormalMovement;
    public MovementSettings SprintingSettings;
    public MovementSettings CrouchedSettings;
    public float SlideSpeedBoost = 2;

    public bool AllowAirMovement = true;
    public float AirAcceleration = 2;
    public float RotationRate = 360;
    public float FPRotationRate = 20;
    public float CoyoteTime = 0.05f;
    public float SprintAngleThreshold = 15f;
    public float SprintInputThreshold = 0.3f;

    [SerializeField]
    private Cinemachine.CinemachineFreeLook freeLookCam;
    [SerializeField]
    private Cinemachine.CinemachineVirtualCamera[] firstPersonCameras;
    [SerializeField] private Transform shoulderPivot;

    private new Transform transform;
    private Rigidbody rigid;
    private Animator anim;
    private new CapsuleCollider collider;
    private CharacterInputController inputController;

    private bool _aiming;
    public bool Aiming
    {
        get => AimMode == eAimType.FirstPerson ? true : _aiming;
        set => _aiming = value;
    }
    private bool rightCam;

    #region Ground Stuff
    private GroundData groundData;
    private bool stairInteraction;
    private bool grounded => groundData.Ground != null || stairInteraction;
    private Vector3 relativeVelocity => groundData.Rigidbody ? (rigid.velocity - groundData.Rigidbody.velocity) : rigid.velocity;

    #endregion

    #region Air Stuff
    private Vector3 airAccel;
    #endregion

    private Vector3 desiredVelocity;
    private Coroutine coyoteWait;

    #region Input Cache
    private Vector2 inputVec;
    private Vector2 lookDelta;
    #endregion

    private void Awake()
    {
        this.transform = GetComponent<Transform>();
        rigid = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        collider = GetComponent<CapsuleCollider>();
        inputController = GetComponentInParent<CharacterInputController>();
        if (MovementReference == null)
        {
            // For First Person
            MovementReference = transform;
        }
        groundData.OnGroundChanged += GroundData_OnGroundChanged;
    }

    private void Start()
    {
        AimMode = _aimMode;
        CurrentMovementMode = _currentMovementMode;
        inputController.Actions.Map.Movement.canceled += OnMovement;
        inputController.Actions.Map.Movement.performed += OnMovement;
    }

    private void OnEnable()
    {
        // Hook input events
        inputController.Actions.Map.Look.canceled += OnLook;
        inputController.Actions.Map.Aim.canceled += OnAim;

        inputController.Actions.Map.Jump.performed += OnJump;
        inputController.Actions.Map.Look.performed += OnLook;
        inputController.Actions.Map.Aim.performed += OnAim;
        inputController.Actions.Map.ChangeShoulder.performed += OnChangeShoulder;
        inputController.Actions.Map.Sprint.performed += OnSprint;
        inputController.Actions.Map.Crouch.performed += OnCrouch;

        CamAnimator?.SetInteger(AnimatorConstants.AimingMode, (int)_aimMode);
        CamAnimator?.SetInteger(AnimatorConstants.MovementMode, (int)_currentMovementMode);
    }

    private void OnDisable()
    {
        inputController.Actions.Map.Look.canceled -= OnLook;
        inputController.Actions.Map.Aim.canceled -= OnAim;

        inputController.Actions.Map.Jump.performed -= OnJump;
        inputController.Actions.Map.Look.performed -= OnLook;
        inputController.Actions.Map.Aim.performed -= OnAim;
        inputController.Actions.Map.ChangeShoulder.performed -= OnChangeShoulder;
        inputController.Actions.Map.Sprint.performed -= OnSprint;
        inputController.Actions.Map.Crouch.performed -= OnCrouch;
    }

    private void OnDestroy()
    {
        inputController.Actions.Map.Movement.canceled -= OnMovement;
        inputController.Actions.Map.Movement.performed -= OnMovement;
    }

    private void OnCrouch(InputAction.CallbackContext obj)
    {
        if (grounded)
        {
            switch (CurrentMovementMode)
            {
                case eMovementMode.Sprinting:
                    CurrentMovementMode = eMovementMode.Sliding;
                    rigid.velocity += transform.forward * SlideSpeedBoost;
                    break;
                case eMovementMode.Normal:
                    CurrentMovementMode = eMovementMode.Crouched;
                    break;
                case eMovementMode.Sliding:
                    CurrentMovementMode = eMovementMode.Crouched;
                    break;
                case eMovementMode.Crouched:
                    CurrentMovementMode = eMovementMode.Normal;
                    break;
                default:
                    break;
            }
        }
    }

    private void OnSprint(InputAction.CallbackContext obj)
    {
        switch (CurrentMovementMode)
        {
            case eMovementMode.Sprinting:
                CurrentMovementMode = eMovementMode.Normal;
                break;
            case eMovementMode.Sliding:
            case eMovementMode.Normal:
            case eMovementMode.Crouched:
                CurrentMovementMode = eMovementMode.Sprinting;
                break;
            default:
                break;
        }
    }

    private void SlideFinished()
    {
        CurrentMovementMode = eMovementMode.Crouched;
    }


    #region Process Input
    public void OnMovement(InputAction.CallbackContext context)
    {
        inputVec = context.ReadValue<Vector2>();
    }

    private void processMovementInput()
    {
        Vector3 forward = MovementReference.forward;
        forward.y = 0;
        forward.Normalize();
        Vector3 right = MovementReference.right;
        Vector3 directionalizedInput = forward * inputVec.y + right * inputVec.x;
        desiredVelocity = CurrentMovementSettings.MaxSpeed * directionalizedInput;

        airAccel = directionalizedInput * AirAcceleration;

        if (CurrentMovementMode == eMovementMode.Sprinting)
        {
            if ((Vector3.Dot(transform.forward, directionalizedInput) < Mathf.Cos(Mathf.Deg2Rad * SprintAngleThreshold)
                    && Aiming)
                || directionalizedInput.sqrMagnitude < SprintInputThreshold * SprintInputThreshold)
            {
                CurrentMovementMode = eMovementMode.Normal;
            }
        }
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        if (grounded)
        {
            Unground();
            rigid.AddForce(transform.up * CurrentMovementSettings.JumpSpeed, ForceMode.VelocityChange);
            anim?.SetTrigger(AnimatorConstants.Jump);
        }
    }

    public void OnLook(InputAction.CallbackContext context)
    {
        lookDelta = context.ReadValue<Vector2>();
        freeLookCam.m_XAxis.m_InputAxisValue = lookDelta.x;
        freeLookCam.m_YAxis.m_InputAxisValue = lookDelta.y;
    }

    public void OnAim(InputAction.CallbackContext context)
    {
        Aiming = context.ReadValue<float>() > 0.5f;
        anim?.SetBool(AnimatorConstants.Aiming, Aiming);
        CamAnimator?.SetBool(AnimatorConstants.Aiming, Aiming);
        if (Aiming)
        {
            transform.rotation = Quaternion.Euler(0, freeLookCam.m_XAxis.Value, 0);
        }
        else
        {
            freeLookCam.m_XAxis.Value = transform.localEulerAngles.y;
        }
    }

    public void OnChangeShoulder(InputAction.CallbackContext context)
    {
        if (Aiming
            && context.ReadValue<float>() > 0.5f)
        {
            rightCam = !rightCam;
            int target = 1;
            int source = 0;
            if (rightCam)
            {
                target = 0;
                source = 1;
            }
            anim?.SetBool(AnimatorConstants.RightSholderCam, rightCam);
            CamAnimator?.SetBool(AnimatorConstants.RightSholderCam, rightCam);
        }
    }
    #endregion

    private void ProcessAiming()
    {
        switch (AimMode)
        {
            case eAimType.FirstPerson:
                rigid.rotation *= Quaternion.AngleAxis(FPRotationRate * Time.fixedDeltaTime * lookDelta.x, transform.up);
                foreach (var cam in firstPersonCameras)
                {
                    cam.GetCinemachineComponent<Cinemachine.CinemachinePOV>().m_VerticalAxis.m_InputAxisValue = lookDelta.y;
                }
                break;
            case eAimType.ThirdPerson:
                if (desiredVelocity.sqrMagnitude > 0)
                {
                    Quaternion targetRotation = Quaternion.LookRotation(desiredVelocity.normalized, Vector3.up);
                    rigid.rotation = Quaternion.RotateTowards(rigid.rotation, targetRotation, RotationRate * Time.fixedDeltaTime);
                }
                break;
            case eAimType.Hybrid:
                if (Aiming)
                {
                    rigid.rotation *= Quaternion.AngleAxis(FPRotationRate * Time.deltaTime * lookDelta.x, transform.up);
                    shoulderPivot.rotation *= Quaternion.Euler(FPRotationRate * Time.deltaTime * lookDelta.y, 0, 0);
                }
                else
                {
                    if (desiredVelocity.sqrMagnitude > 0)
                    {
                        Quaternion targetRotation = Quaternion.LookRotation(desiredVelocity.normalized, Vector3.up);
                        rigid.rotation = Quaternion.RotateTowards(rigid.rotation, targetRotation, RotationRate * Time.fixedDeltaTime);
                    }
                }
                break;
        }
    }

    private void FixedUpdate()
    {
        ProcessAiming();

        if (CurrentMovementMode != eMovementMode.Sliding)
        {
            processMovementInput();

            Vector3 movementVel = relativeVelocity;
            if (grounded)
            {
                #region Standard Locomotion
                Vector3 rotatedDesire = Quaternion.FromToRotation(Vector3.up, groundData.NormalVector) * desiredVelocity;
                Vector3 velDif = rotatedDesire - movementVel;
                if (velDif.y < 0)
                {
                    // Don't stick to falling earth, do get pushed upwards
                    // Note: The push upwards may not be great
                    velDif.y = 0;
                }
                float3 velDifNormalized = velDif.normalized;
                float potentialFriction = groundData.PhysicMaterial.staticFriction * length(groundData.NormalForce);
                Vector3 surfaceFriction = -potentialFriction * movementVel.normalized;
                float maxWalkingForce = Mathf.Min(potentialFriction, CurrentMovementSettings.MaxAcceleration * rigid.mass);

                float3 finalForce = clamp(velDif.magnitude, 0, 1) * maxWalkingForce * velDifNormalized;
                finalForce -= float3(groundData.NormalForce.x,0,groundData.NormalForce.z);
                

                rigid.AddForce(finalForce);
                #endregion

                #region Stairs
                //if (velDif.sqrMagnitude > 0.01f) // Only mess with this is the player is moving
                //{
                //    stairInteraction = false;
                //    float stairHeight = CurrentMovementSettings.StairHeightTolerance;
                //    RaycastHit hit;
                //    Debug.DrawLine(rigid.position + stairHeight * Vector3.up + rigid.velocity, rigid.position + -stairHeight * Vector3.up + rigid.velocity, Color.green);
                //    if (Physics.BoxCast(rigid.position + stairHeight * Vector3.up + rigid.velocity,
                //            new Vector3(collider.radius, 0.03f, 0.03f),
                //            Vector3.down,
                //            out hit,
                //            Quaternion.LookRotation(transform.forward, transform.up),
                //            stairHeight*2))
                //    {
                //        float heightDif = hit.point.y - rigid.position.y;
                //        if (heightDif > 0)
                //        {
                //            rigid.AddForce(Vector3.up * heightDif * CurrentMovementSettings.StairForceMultiplier, ForceMode.Acceleration);
                //            stairInteraction = true;
                //        }
                //    }
                //}
                #endregion
            }
            else
            {
                if (AllowAirMovement)
                {
                    rigid.AddForce(airAccel, ForceMode.Acceleration);
                }
            }
        }

        #region Update Animator Velocity
        if (anim != null)
        {
            if (inputVec.sqrMagnitude > 0)
            {
                Vector3 normalizedMoveVel = transform.InverseTransformDirection(desiredVelocity) / CurrentMovementSettings.MaxSpeed;
                anim.SetFloat(AnimatorConstants.NormalizedVelocityX, normalizedMoveVel.x);
                anim.SetFloat(AnimatorConstants.NormalizedVelocityY, normalizedMoveVel.y);
                anim.SetFloat(AnimatorConstants.NormalizedVelocityZ, normalizedMoveVel.z);
            }
            else
            {
                anim.SetFloat(AnimatorConstants.NormalizedVelocityX, 0);
                anim.SetFloat(AnimatorConstants.NormalizedVelocityY, 0);
                anim.SetFloat(AnimatorConstants.NormalizedVelocityZ, 0);
            }
        }
        #endregion
    }

    private void OnCollisionStay(Collision collision)
    {
        if (Vector3.Dot(collision.contacts[0].normal, transform.up) > 1 - Mathf.Sin(CurrentMovementSettings.SlopeGroundingTolerance * Mathf.Deg2Rad))
        {
            groundData.CollideWithGround(collision, DefaultPhysicMaterial);
        }
        
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.gameObject == groundData.Ground)
        {
            if (coyoteWait == null
                && grounded)
            {
                coyoteWait = StartCoroutine(CoyoteWait());
            }
        }
    }

    private IEnumerator CoyoteWait()
    {
        yield return new WaitForSeconds(CoyoteTime);
        Unground();
        coyoteWait = null;
    }

    private void GroundData_OnGroundChanged(GameObject ground)
    {
        anim?.SetBool(AnimatorConstants.Grounded, grounded);
        if (ground != null
            && coyoteWait != null)
        {
            StopCoroutine(coyoteWait);
            coyoteWait = null;
        }
    }

    public void Unground()
    {
        groundData.Clear();
        if (CurrentMovementMode == eMovementMode.Sliding
            || CurrentMovementMode == eMovementMode.Crouched)
        {
            CurrentMovementMode = eMovementMode.Normal;
        }
    }
}
