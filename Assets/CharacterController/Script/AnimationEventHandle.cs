﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventHandle : MonoBehaviour
{
    public delegate void AnimationEventDelegate(string eventName);
    public event AnimationEventDelegate OnAnimationEvent;

    private void Dispatch(string eventName)
    {
        OnAnimationEvent?.Invoke(eventName);
    }
}
