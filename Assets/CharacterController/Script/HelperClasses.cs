﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct AnimatorID
{
    public int ID;
    public string Name;

    public AnimatorID(string name)
    {
        Name = name ?? throw new ArgumentNullException(nameof(name));
        ID = Animator.StringToHash(name);
    }

    public static implicit operator AnimatorID(string name) => new AnimatorID(name);
    public static implicit operator int(AnimatorID animID) => animID.ID;
}
