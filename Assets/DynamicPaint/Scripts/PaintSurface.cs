﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintSurface : MonoBehaviour
{
    public static int PaintTextureID = Shader.PropertyToID("_PaintTexure");
    public static int SplatParamID = Shader.PropertyToID("_Splat");
    public static int LocationID = Shader.PropertyToID("_Location");
    public static int RotationID = Shader.PropertyToID("_Rotation");
    public static int PaintScaleID = Shader.PropertyToID("_SplatScale");
    public static int TintParamID = Shader.PropertyToID("_Tint");

    public Shader PaintLogic;
    public float UVDensity = 1;
    public float texelDensity = 128;

    RenderTexture buffer1;
    Material paintMat;

    private void Awake()
    {
        int dimension = (int)(texelDensity / UVDensity);
        buffer1 = new CustomRenderTexture(dimension, dimension);
        paintMat = new Material(PaintLogic);

        GetComponent<Renderer>().material.SetTexture(PaintTextureID, buffer1);
    }

    private void OnDestroy()
    {
        buffer1.Release();
    }

    private IEnumerator Start()
    {
        yield return new WaitUntil(() => buffer1.IsCreated());
        Clear();
    }

    public void Clear()
    {
        paintMat.SetVector(LocationID, Vector2.one * 1000);
    }

    public void InitSplat(Texture splat, float size, float rotation, Color tint)
    {
        paintMat.SetTexture(SplatParamID, splat);
        paintMat.SetFloat(PaintScaleID, size * UVDensity);
        paintMat.SetFloat(RotationID, rotation);
        paintMat.SetColor(TintParamID, tint);
    }

    public void Splat(Vector2 location)
    {
        paintMat.SetVector(LocationID, location);
        Graphics.Blit(null, buffer1, paintMat);
    }

    public Vector4 SamplePaint(Vector2 uv)
    {
        return PaintingHelper.Instance.SampleColors(buffer1, uv)[0];
    }
}
