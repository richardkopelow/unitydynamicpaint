﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintingHelper
{
    public static LayerMask PaintCollisionMask =
        LayerMask.GetMask("Paintable", "PaintableDynamic");

    private const int ColorSamplesCount = 20;

    private static PaintingHelper _instance;
    public static PaintingHelper Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new PaintingHelper();
            }
            return _instance;
        }
        set { _instance = value; }
    }

    private ComputeShader samplerShader;
    private int samplerKernal;
    private ComputeBuffer colorResults;
    private Vector4[] managedColorResults = new Vector4[ColorSamplesCount];

    private PaintingHelper()
    {
        samplerShader = (ComputeShader)Resources.Load("TextureSampler");
        samplerKernal = samplerShader.FindKernel("SampleTexture");
        colorResults = new ComputeBuffer(ColorSamplesCount, sizeof(float) * 4, ComputeBufferType.Structured);
        samplerShader.SetBuffer(samplerKernal, "_Results", colorResults);
        Application.quitting += Application_quitting;
    }

    private void Application_quitting()
    {
        colorResults.Dispose();
    }

    public Vector4[] SampleColors(RenderTexture paintTexture, Vector2 uv)
    {
        samplerShader.SetTexture(samplerKernal, "_PaintTex", paintTexture);
        samplerShader.SetVector("_SampleData", uv);
        samplerShader.Dispatch(samplerKernal, 1, 1, 1);
        colorResults.GetData(managedColorResults);
        return managedColorResults;
    }
}
