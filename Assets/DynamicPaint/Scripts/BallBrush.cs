﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBrush : MonoBehaviour
{
    public Texture Splat;
    public float Size;
    private Color tint = Color.white;
    public GameObject ParticlesOnHit;

    private void Start()
    {
        tint = GetComponent<Renderer>().sharedMaterial.GetColor("_BaseColor");
    }

    private void OnCollisionStay(Collision collision)
    {
        PaintSurface canvas = collision.collider.GetComponentInParent<PaintSurface>();
        if (canvas != null)
        {
            canvas.InitSplat(Splat, Size, Random.value, tint);
            RaycastHit hit;
            if (Physics.Raycast(transform.position, collision.contacts[0].point - transform.position, out hit, 0.4f, PaintingHelper.PaintCollisionMask))
            {
                // Paint!
                canvas.Splat(hit.textureCoord2);

                // Particles!
                if (ParticlesOnHit != null)
                {
                    Instantiate(ParticlesOnHit, collision.contacts[0].point, Quaternion.LookRotation(transform.position - collision.contacts[0].point));
                }

                // Nuke!
                Destroy(gameObject);
            }
        }
    }
}
