﻿Shader "DynamicPain/PaintLogic"
{
	Properties
	{
		_Splat("Splat Texture", 2D) = "white" {}
		_Location("Location", Vector) = (0.5, 0.5, 0, 0)
	}

		SubShader
		{
			Cull Off
			ZWrite Off
			ZTest Always
		   Lighting Off
			BlendOp Add, Max
		   Blend SrcAlpha OneMinusSrcAlpha, One One

		   Pass
		   {
			   CGPROGRAM
			   #pragma vertex vert
			   #pragma fragment frag
			   #pragma target 3.0

			   #include "UnityCG.cginc"

			   sampler2D _MainTex;
			   sampler2D _Splat;
			   uniform float4 _Location;
			   float _Rotation;
			   float _SplatScale;
			   float4 _Tint;

			   struct appdata_t {
				   float4 vertex : POSITION;
				   float2 texcoord : TEXCOORD0;
				   UNITY_VERTEX_INPUT_INSTANCE_ID
			   };

			   struct v2f
			   {
				   float4 vertex : SV_POSITION;
				   float2 texcoord : TEXCOORD0;
			   };

			   v2f vert(appdata_t v)
			   {
				   v2f o;
				   o.vertex = UnityObjectToClipPos(v.vertex);
				   o.texcoord = v.texcoord;
				   return o;
			   }

			   float2 GetSplatUV(float2 canvasUV)
			   {
				   float2 uv = (canvasUV - _Location);
				   float2 rotatedUV = float2(0, 0);
				   rotatedUV.x = cos(_Rotation) * uv.x + sin(_Rotation) * uv.y;
				   rotatedUV.y = -sin(_Rotation) * uv.x + cos(_Rotation) * uv.y;
				   rotatedUV /= _SplatScale;
				   return rotatedUV + 0.5;
			   }

			   float4 frag(v2f IN) : COLOR
			   {
				   float2 splatUV = GetSplatUV(IN.texcoord.xy);
				   clip((splatUV.x > 0 && splatUV.x < 1 && splatUV.y > 0 && splatUV.y < 1) ? 1 : -1);

					float4 c = tex2D(_Splat, splatUV.xy) * _Tint;

				   return c;
			   }
			   ENDCG
		   }
		}
}