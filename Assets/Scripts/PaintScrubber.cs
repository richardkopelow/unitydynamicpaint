﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintScrubber : MonoBehaviour
{

    private const int paintTick = 3;
    private enum eAIState
    {
        Move,
        Turn
    }

    public Texture2D Splat;
    public float Size = 1;
    public Color PaintColor = Color.red;
    public float MovementSpeed = 1;
    public Renderer BodyRenderer;

    private Rigidbody rigid;
    private eAIState aiState = eAIState.Move;
    private Quaternion targetRotation;
    private float rotationTime = 0;
    private float timeInState = 0;
    private float paintFrameCount = 0;

    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        foreach (Material mat in BodyRenderer.materials)
        {
            mat.SetColor("_EmissionColor", PaintColor * 2.5f);
        }
    }

    private void Update()
    {
        if (paintFrameCount < paintTick)
        {
            paintFrameCount++;
        }
        else
        {
            Paint();
        }
        timeInState += Time.deltaTime;
        switch (aiState)
        {
            case eAIState.Move:
                if (Physics.BoxCast(transform.position + transform.up * 0.1f, new Vector3(0.5f, 0.05f, 0.05f), transform.forward, transform.rotation, 1))
                {
                    timeInState = 0;
                    aiState = eAIState.Turn;
                    float rotation = Random.Range(-180, 180);
                    targetRotation = Quaternion.Euler(0, rotation, 0);
                    rotationTime = Mathf.Abs(rotation / 180 * 3);
                }
                else
                {
                    rigid.MovePosition(rigid.position + transform.forward * MovementSpeed * Time.deltaTime);
                }
                break;
            case eAIState.Turn:
                {
                    rigid.MoveRotation(Quaternion.Slerp(transform.rotation, targetRotation, timeInState / rotationTime));
                    if (timeInState > rotationTime)
                    {
                        timeInState = 0;
                        aiState = eAIState.Move;
                    }
                }
                break;
            default:
                break;
        }
    }

    private void Paint()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position + transform.up * 0.1f, -transform.up, out hit, 0.4f, PaintingHelper.PaintCollisionMask))
        {
            PaintSurface canvas = hit.collider.GetComponentInParent<PaintSurface>();
            canvas.InitSplat(Splat, Size, Random.value, PaintColor);
            // Paint!
            canvas.Splat(hit.textureCoord2);
        }
    }
}
