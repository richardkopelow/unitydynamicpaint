﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintDiveController : MonoBehaviour
{
    private class AnimatorConstants
    {
        public static AnimatorID Swimming = "Swimming";
    }

    public Color CharacterColor = Color.white;
    public GameObject MainCharacter;
    public float JumpOutSpeed = 10;
    public GameObject PaintCharacter;
    public Cinemachine.CinemachineTargetGroup CameraTarget;
    [SerializeField]
    private Material basePaintMaterial;
    public Material PaintMaterial { get; set; }

    private CharacterInputController inputController;
    private Transform mainCharacterTransform;
    private Animator mainCharacterAnimator;
    private InPaintMovementController inPaintMovement;

    private bool inPaint;

    private void Awake()
    {
        inputController = GetComponentInParent<CharacterInputController>();
        mainCharacterTransform = MainCharacter.transform;
        mainCharacterAnimator = MainCharacter.GetComponent<Animator>();
        inPaintMovement = PaintCharacter.GetComponent<InPaintMovementController>();
        PaintMaterial = Instantiate(basePaintMaterial);
        PaintMaterial.SetColor("_BaseColor", CharacterColor);
    }

    private void OnEnable()
    {
        inputController.Actions.Map.Dive.started += Dive_started;
        inputController.Actions.Map.Dive.canceled += Dive_canceled;
    }

    private void Start()
    {
        PaintCharacter.SetActive(false);
        MainCharacter.GetComponent<AnimationEventHandle>().OnAnimationEvent += PaintDiveController_OnAnimationEvent;
    }

    private void OnDestroy()
    {
        MainCharacter.GetComponent<AnimationEventHandle>().OnAnimationEvent -= PaintDiveController_OnAnimationEvent;
    }

    private void PaintDiveController_OnAnimationEvent(string eventName)
    {
        switch (eventName)
        {
            case "Dove":
                {
                    inPaint = true;
                    PaintCharacter.transform.position = mainCharacterTransform.position;
                    MainCharacter.SetActive(false);
                    PaintCharacter.SetActive(true);
                    CameraTarget.m_Targets[0].weight = 0;
                    CameraTarget.m_Targets[1].weight = 1;
                }
                break;
            default:
                break;
        }
    }

    private void Dive_started(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        if (PaintCheck(new Ray(mainCharacterTransform.position + Vector3.up * 0.1f, Vector3.down), out _))
        {
            mainCharacterAnimator.SetBool(AnimatorConstants.Swimming, true);
        }
    }

    private void Dive_canceled(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        if (inPaint)
        {
            inPaint = false;
            mainCharacterTransform.position = PaintCharacter.transform.position + inPaintMovement.SurfaceNormal * 0.3f;
            MainCharacter.GetComponent<Rigidbody>().velocity = Vector3.up * JumpOutSpeed;
            MainCharacter.GetComponent<CharacterController>().Unground();
            MainCharacter.SetActive(true);
            PaintCharacter.SetActive(false);
            CameraTarget.m_Targets[0].weight = 1;
            CameraTarget.m_Targets[1].weight = 0;
        }

        mainCharacterAnimator.SetBool(AnimatorConstants.Swimming, false);
    }

    public bool PaintCheck(Ray ray, out RaycastHit hit)
    {
        if (Physics.Raycast(ray, out hit, 0.5f, PaintingHelper.PaintCollisionMask))
        {
            PaintSurface surface = hit.collider.GetComponentInParent<PaintSurface>();
            if (surface != null)
            {
                Vector4 sampledColorV4 = surface.SamplePaint(hit.lightmapCoord);
                Vector4 characterColor = new Vector4(CharacterColor.r, CharacterColor.g, CharacterColor.b, CharacterColor.a);

                if ((sampledColorV4 - characterColor).sqrMagnitude < 0.05f)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
