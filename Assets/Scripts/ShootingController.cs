﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingController : MonoBehaviour
{
    public Camera MainCamera;
    public Transform SpawnPoint;
    public Transform AimIKTarget;
    public UnityEngine.Animations.Rigging.Rig AimingRig;
    public Rigidbody Projectile;
    public float LaunchSpeed = 20;
    public float FireCooldown = 0.05f;
    public LayerMask LayerMask;

    private CharacterInputController inputController;
    private CharacterController characterController;
    private PaintDiveController paintDiveController;
    private Animator animator;

    private bool firing = false;
    private float lastFireTime;

    private void Awake()
    {
        inputController = GetComponentInParent<CharacterInputController>();
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        paintDiveController = GetComponentInParent<PaintDiveController>();
    }

    private void Start()
    {
        inputController.Actions.Map.Fire.canceled += Fire_performed;
        inputController.Actions.Map.Fire.performed += Fire_performed;
    }

    private void OnDestroy()
    {
        inputController.Actions.Map.Fire.canceled -= Fire_performed;
        inputController.Actions.Map.Fire.performed -= Fire_performed;
    }

    private void Fire_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        firing = obj.ReadValue<float>() > 0.5f;

    }

    private void Update()
    {

        if (characterController.Aiming)
        {
            Ray camRay = MainCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            Vector3 targetPos = camRay.GetPoint(10);
            RaycastHit hit;
            if (Physics.Raycast(camRay, out hit, 100, LayerMask.value))
            {
                targetPos = hit.point;
            }
            if (firing
                && Time.time - lastFireTime > FireCooldown)
            {
                Rigidbody projectile = Instantiate(Projectile, SpawnPoint.position, Quaternion.identity);
                projectile.velocity = (targetPos - SpawnPoint.position).normalized * LaunchSpeed;
                projectile.GetComponent<Renderer>().material = paintDiveController.PaintMaterial;

                lastFireTime = Time.time;
                Debug.DrawRay(camRay.origin, camRay.direction, Color.red, 0.1f, false);
            }
            AimIKTarget.position = targetPos;
            AimingRig.weight = 1;
        }
        else
        {
            AimingRig.weight = 0;
        }
    }
}
