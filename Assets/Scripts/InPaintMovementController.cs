﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InPaintMovementController : MonoBehaviour
{
    public Animator CamAnimator;
    [SerializeField]
    private Cinemachine.CinemachineFreeLook freeLookCam;
    public Transform MovementReference;
    public float MovementSpeed = 10;


    [System.NonSerialized]
    public Vector3 SurfaceNormal;

    private CharacterInputController inputController;
    private PaintDiveController paintDiveController;
    private Rigidbody rigid;
    private Vector3 directionalizedInput;
    private Rigidbody kineticGround;
    private Vector3 kineticGroundOffset;
    private Vector2 lookDelta;
    private Vector2 inputVec;

    private void Awake()
    {
        inputController = GetComponentInParent<CharacterInputController>();
        paintDiveController = GetComponentInParent<PaintDiveController>();
        rigid = GetComponent<Rigidbody>();

        inputController.Actions.Map.Movement.performed += Movement_performed;
        inputController.Actions.Map.Movement.canceled += Movement_performed;
    }

    private void Start()
    {
        var particles = GetComponent<ParticleSystemRenderer>();
        particles.material = paintDiveController.PaintMaterial;
        foreach (var renderer in GetComponentsInChildren<Renderer>())
        {
            renderer.material.SetColor("_BaseColor", paintDiveController.CharacterColor);
        }
    }

    private void OnEnable()
    {
        RaycastHit hit;
        Ray searchRay = new Ray();
        searchRay.origin = transform.position + Vector3.up * 0.1f;
        searchRay.direction = Vector3.down;
        if (Physics.Raycast(searchRay, out hit, 0.2f, PaintingHelper.PaintCollisionMask))
        {
            SurfaceNormal = hit.normal;
            kineticGround = hit.collider.GetComponentInParent<Rigidbody>();
            if (kineticGround)
            {
                kineticGroundOffset = kineticGround.transform.InverseTransformPoint(transform.position);
            }
        }

        if (CamAnimator != null)
        {
            CamAnimator.SetInteger(CharacterController.AnimatorConstants.AimingMode, (int)CharacterController.eAimType.ThirdPerson);
            CamAnimator.SetInteger(CharacterController.AnimatorConstants.MovementMode, (int)CharacterController.eMovementMode.Normal);
            CamAnimator.SetBool(CharacterController.AnimatorConstants.Aiming, false);
        }

        inputController.Actions.Map.Look.performed += OnLook;
        inputController.Actions.Map.Look.canceled += OnLook;
    }

    private void OnDisable()
    {
        inputController.Actions.Map.Look.performed -= OnLook;
        inputController.Actions.Map.Look.canceled -= OnLook;
    }

    private void OnDestroy()
    {
        inputController.Actions.Map.Movement.performed -= Movement_performed;
        inputController.Actions.Map.Movement.canceled -= Movement_performed;
        inputController.Actions.Map.Look.performed -= OnLook;
        inputController.Actions.Map.Look.canceled -= OnLook;
    }

    private void Movement_performed(InputAction.CallbackContext obj)
    {
        inputVec = obj.ReadValue<Vector2>();
    }

    private void processMovementInput()
    {
        Vector3 forward = MovementReference.forward;
        forward.y = 0;
        forward.Normalize();
        Vector3 right = MovementReference.right;
        directionalizedInput = forward * inputVec.y + right * inputVec.x;
    }

    public void OnLook(InputAction.CallbackContext context)
    {
        lookDelta = context.ReadValue<Vector2>();
        freeLookCam.m_XAxis.m_InputAxisValue = lookDelta.x;
        freeLookCam.m_YAxis.m_InputAxisValue = lookDelta.y;
    }

    private void FixedUpdate()
    {
        processMovementInput();

        Transform kineticGroundTransform = null;
        if (kineticGround)
        {
            kineticGroundTransform = kineticGround.transform;
            transform.position = kineticGroundTransform.TransformPoint(kineticGroundOffset);
        }

        Quaternion upRotation = Quaternion.FromToRotation(Vector3.up, SurfaceNormal);
        if (directionalizedInput.sqrMagnitude > 0)
        {
            RaycastHit hit;
            Vector3 desiredStep = (upRotation * directionalizedInput) * MovementSpeed * Time.deltaTime;
            Ray searchRay = new Ray();
            searchRay.origin = transform.position + SurfaceNormal * 0.2f;
            searchRay.direction = (desiredStep + transform.position - searchRay.origin).normalized;
            if (paintDiveController.PaintCheck(searchRay, out hit))
            {
                SurfaceNormal = hit.normal;
                transform.position = hit.point;
                kineticGround = hit.collider.GetComponentInParent<Rigidbody>();
            }
        }

        if (kineticGround)
        {
            kineticGroundOffset = kineticGround.transform.InverseTransformPoint(transform.position);
        }

    }
}
